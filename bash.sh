#!/bin/bash
cd /home/antoine/soft/docker/ros/ros

CURRENT_UID=$(id -u):$(id -g)
docker-compose exec -u $CURRENT_UID ros bash
