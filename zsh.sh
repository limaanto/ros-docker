#!/bin/bash
cd /home/antoine/soft/docker/ros/ros
CURRENT_UID=$(id -u):$(id -g)
optirun docker-compose exec -u $CURRENT_UID ros zsh -c "cd ~; exec zsh"
