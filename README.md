# ROS Docker container

Docker container to develop and run ROS applications
Features :
  - Ubuntu xenial based
  - ROS kinetic (ros-kinetic-desktop)
  - X11 enabled
  - Provide a ros user which have the same uid and gid as the host user

Requiere X11, xhost, docker and docker-compose

## Run the container

Run `./docker.sh` in terminal. This will run the container and start the ros master (roscore).

## Open a new shell in the container

Run `./bash_ros.sh` in a terminal. This will login as the ros user and open bash shell.

## Install new packages on the container

Add installation command at the end of the `./ros/Dokerfile` (apt command, etc...).

Then rebuild the container by running `sudo docker-compose build`.

## Change .bashrc of the ros user

The .bashrc for the ros user is available at `./ros/.bashrc` and can be modified.

## Add volumes to the container

Add new volumes in the end of `./docker-compose.yml`.
