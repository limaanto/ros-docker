#!/bin/bash
cd /home/antoine/soft/docker/ros/ros
systemctl start docker
xhost +local:docker # allow docker to access x11
HOST_UID=$(id -u)
HOST_GID=$(id -g)
HOST_UID=$HOST_UID HOST_GID=$HOST_GID docker-compose up
xhost -local:docker # remove access to x11
